//
//  Season.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/19/21.
//

import Foundation
import RealmSwift
import Unrealm

// MARK: - Season
struct Season: Realmable, Codable, Hashable{
    var airDate: String? = ""
    var episodeCount: Int = 0, id: Int = 0
    var name: String = "", overview: String = ""
    var posterPath: String? = ""
    var seasonNumber: Int = 0

    enum CodingKeys: String, CodingKey {
        case airDate = "air_date"
        case episodeCount = "episode_count"
        case id, name, overview
        case posterPath = "poster_path"
        case seasonNumber = "season_number"
    }
}
