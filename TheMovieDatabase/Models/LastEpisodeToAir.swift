//
//  LastEpisodeToAir.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/19/21.
//


import Foundation
import RealmSwift
import Unrealm
// MARK: - LastEpisodeToAir
struct LastEpisodeToAir: Realmable, Codable {
    var airDate: String = ""
    var episodeNumber: Int = 0, id: Int = 0
    var name: String = "", overview: String = "", productionCode: String = ""
    var seasonNumber: Int = 0
    var stillPath: String? = ""
    var voteAverage: Double = 0.0
    var voteCount: Int = 0

    enum CodingKeys: String, CodingKey {
        case airDate = "air_date"
        case episodeNumber = "episode_number"
        case id, name, overview
        case productionCode = "production_code"
        case seasonNumber = "season_number"
        case stillPath = "still_path"
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
}
