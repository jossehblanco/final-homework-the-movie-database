//
//  Episode.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/20/21.
//


import Foundation
import RealmSwift
import Unrealm

// MARK: - Episode
struct Episode: Realmable, Codable, Hashable{
    var airDate: String = ""
    var episodeNumber: Int = 0
    var crew: [Crew] = []
    var guestStars: [Crew] = []
    var id: Int = 0
    var name: String = ""
    var overview: String = ""
    var productionCode: String = ""
    var seasonNumber: Int = 0
    var stillPath: String? = ""
    var voteAverage: Double = 0.0
    var voteCount: Int = 0

    enum CodingKeys: String, CodingKey {
        case airDate = "air_date"
        case episodeNumber = "episode_number"
        case crew
        case guestStars = "guest_stars"
        case id, name, overview
        case productionCode = "production_code"
        case seasonNumber = "season_number"
        case stillPath = "still_path"
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
}
