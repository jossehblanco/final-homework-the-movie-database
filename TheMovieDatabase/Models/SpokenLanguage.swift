//
//  SpokenLanguage.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/19/21.
//

import Foundation
import RealmSwift
import Unrealm
// MARK: - SpokenLanguage
struct SpokenLanguage: Realmable, Codable {
    var englishName: String = "", iso639_1: String = "", name: String = ""

    enum CodingKeys: String, CodingKey {
        case englishName = "english_name"
        case iso639_1 = "iso_639_1"
        case name
    }
}
