//
//  Session.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/14/21.
//

import Foundation
import RealmSwift
import Unrealm

struct Session: Realmable, Codable{
    var success: Bool = false
    var session_id: String = ""
}
