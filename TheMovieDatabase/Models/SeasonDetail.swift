//
//  SeasonDetail.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/20/21.
//


// Welcome.swift

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   var Welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation
import RealmSwift
import Unrealm

// MARK: - Welcome
struct SeasonDetail: Realmable, Codable, Hashable{
    var _id: String = ""
    var airDate: String = ""
    var episodes: [Episode] = []
    var name: String = ""
    var overview: String = ""
    var id: Int = 0
    var posterPath: String? = ""
    var seasonNumber: Int = 0

    enum CodingKeys: String, CodingKey {
        case _id = "_id"
        case airDate = "air_date"
        case episodes, name, overview
        case id = "id"
        case posterPath = "poster_path"
        case seasonNumber = "season_number"
    }
}

