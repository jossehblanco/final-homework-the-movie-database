//
//  CreatedBy.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/19/21.
//

import Foundation
import RealmSwift
import Unrealm

// MARK: - CreatedBy
struct CreatedBy: Realmable, Codable {
    var id: Int = 0
    var creditID: String = "", name: String = ""
    var gender: Int = 0
    var profilePath: String? = ""

    enum CodingKeys: String, CodingKey {
        case id
        case creditID = "credit_id"
        case name, gender
        case profilePath = "profile_path"
    }
}
