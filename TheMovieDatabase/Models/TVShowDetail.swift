//
//  TVShowDetail.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/19/21.
//

import Foundation

// Welcome.swift

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   var Welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation
import RealmSwift
import Unrealm

// MARK: - TVShowDetail
struct TVShowDetail: Realmable, Codable {
    var backdropPath: String = ""
    var createdBy: [CreatedBy] = []
    var episodeRunTime: [Int] = []
    var firstAirDate: String = ""
    var genres: [Genre] = []
    var homepage: String = ""
    var id: Int = 0
    var inProduction: Bool = false
    var languages: [String] = []
    var lastAirDate: String = ""
    var lastEpisodeToAir: LastEpisodeToAir = LastEpisodeToAir()
    var name: String = ""
    var networks: [Network] = []
    var numberOfEpisodes: Int = 0, numberOfSeasons: Int = 0
    var originCountry: [String] = []
    var originalLanguage: String = "", originalName: String = "", overview: String = ""
    var popularity: Double = 0.0
    var posterPath: String = ""
    var productionCompanies: [Network] = []
    var productionCountries: [ProductionCountry] = []
    var seasons: [Season] = []
    var spokenLanguages: [SpokenLanguage] = []
    var status: String = "", tagline: String = "", type: String = ""
    var voteAverage: Double = 0
    var voteCount: Int = 0
    var cacheKey = ""

    enum CodingKeys: String, CodingKey {
        case backdropPath = "backdrop_path"
        case createdBy = "created_by"
        case episodeRunTime = "episode_run_time"
        case firstAirDate = "first_air_date"
        case genres, homepage, id
        case inProduction = "in_production"
        case languages
        case lastAirDate = "last_air_date"
        case lastEpisodeToAir = "last_episode_to_air"
        case name
        case networks
        case numberOfEpisodes = "number_of_episodes"
        case numberOfSeasons = "number_of_seasons"
        case originCountry = "origin_country"
        case originalLanguage = "original_language"
        case originalName = "original_name"
        case overview, popularity
        case posterPath = "poster_path"
        case productionCompanies = "production_companies"
        case productionCountries = "production_countries"
        case seasons
        case spokenLanguages = "spoken_languages"
        case status, tagline, type
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
    
    static func primaryKey() -> String? {
        return "id"
    }
}
