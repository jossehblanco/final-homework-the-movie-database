//
//  EpisodeDetail.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/20/21.
//

import Foundation
import RealmSwift
import Unrealm

struct EpisodeDetail: Realmable, Codable{
    var airDate: String = ""
    var crew: [Crew] = []
    var episodeNumber: Int = 0
    var guestStars: [GuestStar] = []
    var name: String = ""
    var overview: String = ""
    var id: Int = 0
    var productionCode: String? = ""
    var seasonNumber: Int = 0
    var stillPath: String? = ""
    var voteAverage: Double = 0.0
    var voteCount: Int = 0
    var cacheKey = ""
    
    enum CodingKeys: String, CodingKey {
        case airDate = "air_date"
        case episodeNumber = "episode_number"
        case guestStars = "guest_stars"
        case productionCode = "production_code"
        case seasonNumber = "season_number"
        case stillPath = "still_path"
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
        case crew, name, overview, id
        
    }
    static func primaryKey() -> String? {
        return "id"
    }
}

struct GuestStar: Realmable, Codable, Hashable{
    var id: Int = 0
    var name: String = ""
    var creditId: String = ""
    var character: String = ""
    var profilePath: String? = ""
    
    enum CodingKeys: String, CodingKey {
        case creditId = "credit_id"
        case profilePath = "profile_path"
        case id, name, character
    }
    static func primaryKey() -> String? {
        return "id"
    }
}
