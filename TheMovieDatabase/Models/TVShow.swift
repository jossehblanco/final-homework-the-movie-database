//
//  TVShow.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/17/21.
//

import Foundation
import RealmSwift
import Unrealm
	
struct TVShow: Realmable, Codable, Equatable, Hashable, Identifiable{
    let uuid = UUID().uuidString
    var posterPath: String? = ""
    var popularity: Double = 0.0
    var id: Int = 0
    var backdropPath: String? = ""
    var voteAverage: Double = 0.0
    var overview = "", firstAirDate: String = ""
    var originCountry: [String] = []
    var genreIds: [Int] = []
    var originalLanguage: String = ""
    var voteCount: Int = 0
    var name = "", originalName: String = ""
    
    init() {
        
    }
    
    static func ==(lhs: TVShow, rhs: TVShow) -> Bool {
            return lhs.uuid == rhs.uuid
        }

        func hash(into hasher: inout Hasher) {
            hasher.combine(uuid)
        }
    
    enum CodingKeys: String, CodingKey {
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
        case firstAirDate = "first_air_date"
        case voteAverage = "vote_average"
        case originCountry = "origin_country"
        case genreIds = "genre_ids"
        case originalLanguage = "original_language"
        case voteCount = "vote_count"
        case originalName = "original_name"
        case popularity, id, overview, name
    }    
    static func primaryKey() -> String? {
        return "id"
    }
}
