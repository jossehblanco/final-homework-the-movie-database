//
//  SessionRequestBody.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/14/21.
//


import Foundation
import RealmSwift
import Unrealm

struct SessionRequestBody: Realmable, Codable{
    var request_token: String = ""
}
