//
//  ProductionCountry.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/19/21.
//

import Foundation
import RealmSwift
import Unrealm
// MARK: - ProductionCountry
struct ProductionCountry: Realmable, Codable {
    var iso3166_1: String = "", name: String = ""

    enum CodingKeys: String, CodingKey {
        case iso3166_1 = "iso_3166_1"
        case name
    }
}
