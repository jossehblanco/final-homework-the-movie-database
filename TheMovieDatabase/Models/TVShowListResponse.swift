//
//  TVShowListResponse.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/17/21.
//

import Foundation
import RealmSwift
import Unrealm

struct TVShowListResponse: Realmable , Codable {
    let id = UUID().uuidString
    var page: Int = 0
    var results: [TVShow] = []
    var totalResults: Int = 0
    var totalPages: Int = 0
    var cacheKey = ""
    
    enum CodingKeys: String, CodingKey {
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case page = "page"
        case results = "results"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.page = try container.decode(Int.self, forKey: .page)
        self.results = try container.decode([TVShow].self, forKey: .results)
        self.totalResults = try container.decode(Int.self, forKey: .totalResults)
        self.totalPages = try container.decode(Int.self, forKey: .totalPages)
    }
    init() {
        
    }
    
    static func primaryKey() -> String? {
        return "id"
    }
}
