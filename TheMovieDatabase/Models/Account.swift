//
//  Account.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/21/21.
//

import Foundation

// MARK: - patchFaiFaiWelcome
struct Account: Codable {
    var avatar: Avatar = Avatar()
    var id: Int = 0
    var iso639_1: String = "", iso3166_1: String = "", name: String = ""
    var includeAdult: Bool = false
    var username: String = ""

    enum CodingKeys: String, CodingKey {
        case avatar, id
        case iso639_1 = "iso_639_1"
        case iso3166_1 = "iso_3166_1"
        case name
        case includeAdult = "include_adult"
        case username
    }
}

// MARK: - tmdb
struct tmdbAvatar: Codable {
    var avatar_path: String? = ""
}

// MARK: - Avatar
struct Avatar: Codable {
    var gravatar: Gravatar = Gravatar()
    var tmdb: tmdbAvatar?
}


// MARK: - Gravatar
struct Gravatar: Codable {
    var hash: String = ""
}


