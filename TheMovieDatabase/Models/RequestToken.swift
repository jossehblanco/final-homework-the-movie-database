//
//  RequestToken.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/11/21.
//

import Foundation
import RealmSwift
import Unrealm

struct RequestToken: Realmable, Codable{
   var success: Bool = false
   var expires_at: String = ""
   var request_token: String = ""
}
