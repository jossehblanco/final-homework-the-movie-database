//
//  MarkAsFavouriteResponse.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/21/21.
//

import Foundation

struct MarkAsFavouriteResponse: Codable{
    let statusCode: Int
    let statusMessage: String
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case statusMessage = "status_message"
    }
}
