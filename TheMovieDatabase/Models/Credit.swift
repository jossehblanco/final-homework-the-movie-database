//
//  Credit.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/19/21.
//

import Foundation
import RealmSwift
import Unrealm

struct Credit: Realmable, Codable {
    var cast: [Cast] = []
    var crew: [Crew] = []
    var id: Int = 0
    var cacheKey = ""
    
    enum CodingKeys: String, CodingKey {
        case cast, crew, id
    }
    
    static func primaryKey() -> String? {
        return "id"
    }
}
