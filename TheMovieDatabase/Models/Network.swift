//
//  Network.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/19/21.
//

import Foundation
import RealmSwift
import Unrealm
// MARK: - Network
struct Network: Realmable, Codable {
    var name: String = ""
    var id: Int = 0
    var logoPath: String? = ""
    var originCountry: String = ""

    enum CodingKeys: String, CodingKey {
        case name, id
        case logoPath = "logo_path"
        case originCountry = "origin_country"
    }
}
