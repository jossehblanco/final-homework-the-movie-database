//
//  SeasonDetails.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/21/21.
//

import Foundation
import Unrealm

struct RealmSeasonDetails: Realmable{
    init() {
        
    }
    
    var cacheKey = ""
    var seasonDetails: [SeasonDetail] = []
    
    init(seasonDetails: [SeasonDetail]){
        self.seasonDetails = seasonDetails
    }
    
    static func primaryKey() -> String? {
        return "cacheKey"
    }
}
