//
//  Genre.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/19/21.
//

import Foundation
import RealmSwift
import Unrealm
// MARK: - Genre
struct Genre: Realmable, Codable {
    var id: Int = 0
    var name: String = ""
}
