//
//  Crew.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/19/21.
//

import Foundation
import RealmSwift
import Unrealm

struct Crew: Realmable, Codable, Hashable{
        var adult: Bool? = false
        var gender: Int? = 0, id: Int = 0
        var knownForDepartment: String? = "", name: String = "", originalName: String? = ""
        var popularity: Double? = 0.0
        var profilePath: String? = ""
        var character: String? = ""
        var creditID: String = ""
        var department: String? = ""
        var job: String? = ""

        enum CodingKeys: String, CodingKey {
            case adult, gender, id
            case knownForDepartment = "known_for_department"
            case name
            case originalName = "original_name"
            case popularity
            case profilePath = "profile_path"
            case character
            case creditID = "credit_id"
            case department, job
        }
    static func primaryKey() -> String? {
        return "id"
    }
}
