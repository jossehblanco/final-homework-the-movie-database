//
//  SeasonHeaderCollectionReusableView.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/20/21.
//

import UIKit

class SeasonHeaderCollectionReusableView: UICollectionReusableView {
    static var reuseIdentifier: String {
        return "allSeasonsHeader"
      }
 
      lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(
          ofSize: UIFont.preferredFont(forTextStyle: .title3).pointSize,
          weight: .bold)
        label.adjustsFontForContentSizeCategory = true
        label.textColor = .label
        label.textAlignment = .left
        label.numberOfLines = 1
        label.setContentCompressionResistancePriority(
          .defaultHigh,
          for: .horizontal)
        return label
      }()
      
      override init(frame: CGRect) {
        super.init(frame: frame)
        // 3
        backgroundColor = #colorLiteral(red: 0.03921568627, green: 0.08235294118, blue: 0.1058823529, alpha: 1)
        addSubview(titleLabel)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
          NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(
              equalTo: leadingAnchor,
              constant: 5),
            titleLabel.trailingAnchor.constraint(
              lessThanOrEqualTo: trailingAnchor,
              constant: -5)])
        } else {
          NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(
              equalTo: readableContentGuide.leadingAnchor),
            titleLabel.trailingAnchor.constraint(
              lessThanOrEqualTo: readableContentGuide.trailingAnchor)
          ])
        }
        NSLayoutConstraint.activate([
          titleLabel.topAnchor.constraint(
            equalTo: topAnchor,
            constant: 10),
          titleLabel.bottomAnchor.constraint(
            equalTo: bottomAnchor,
            constant: -10)
        ])
        titleLabel.textColor = #colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1)
      }
      
      required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
      }
}
