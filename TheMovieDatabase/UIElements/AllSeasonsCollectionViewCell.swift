//
//  AllSeasonsCollectionViewCell.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/20/21.
//

import UIKit

class AllSeasonsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var seasonName: UILabel!
    @IBOutlet weak var episodeList: UICollectionView!
    
}
