//
//  TVShowCollectionViewCell.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/17/21.
//

import UIKit

class TVShowCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var moviePoster: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var movieDate: UILabel!
    @IBOutlet weak var movieVoteCount: UILabel!
    @IBOutlet weak var movieDescription: UILabel!
    
    func setVoteCountText(to text: String){
        movieVoteCount.text = "★  \(text)"
    }
    
}
