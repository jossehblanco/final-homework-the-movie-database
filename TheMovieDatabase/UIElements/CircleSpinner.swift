//
//  SpinningCircle.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/12/21.
//

import UIKit

class CircleSpinner: UIView {
    let spinningCircle = CAShapeLayer()
    
    override init(frame: CGRect){
        super.init(frame: frame)
        setUpCircleSpinner()
        spinCircle()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setUpCircleSpinner(){
        frame = CGRect(x: 0.0, y: 0.0, width: 50.0, height: 50.0)
        let rect = self.bounds
        let path = UIBezierPath(ovalIn: rect)
        spinningCircle.path = path.cgPath
        spinningCircle.fillColor = UIColor.clear.cgColor
        spinningCircle.strokeColor = #colorLiteral(red: 0.1294117647, green: 0.8039215686, blue: 0.3960784314, alpha: 1).cgColor
        spinningCircle.lineWidth = 10
        spinningCircle.strokeEnd = 0.25
        spinningCircle.lineCap = .round
        self.layer.addSublayer(spinningCircle)
    }
    
    func spinCircle(){
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveLinear, animations: {
            self.transform = CGAffineTransform(rotationAngle: .pi)
        }, completion: { completed in
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveLinear, animations: {
                self.transform = CGAffineTransform(rotationAngle: 0)
            }, completion: { completion in
                self.spinCircle()
            })
        })
    }
}
