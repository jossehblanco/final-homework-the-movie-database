//
//  FavouritesView.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/21/21.
//

import SwiftUI
import Kingfisher

struct FavouritesView: View {
    @ObservedObject var viewModel: FavouriteScreenViewModel
    var body: some View {
        ScrollView{
            VStack(spacing: 24){
                HStack{
                    Text("Profile").font(.title).foregroundColor(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1))).fontWeight(.bold)
                    Spacer()
                }
                HStack(spacing: 48) {
                    KFImage.url(URL(string: viewModel.getURLForAvatar())!).placeholder({
                        Image("TMDb").resizable().frame(width: 150, height: 150, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).scaledToFit() .clipShape(Circle())
                    }).resizable().frame(width: 150, height: 150, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).scaledToFit() .clipShape(Circle())
                    VStack(alignment: .leading){
                        Text(!viewModel.accountDetails.name.isEmpty ? viewModel.accountDetails.name : viewModel.accountDetails.username ).font(.title2).foregroundColor(Color(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))).fontWeight(.bold)
                        Text("@\(viewModel.accountDetails.username)").font(.caption).foregroundColor(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1))).fontWeight(.bold)
                    }
                }
                HStack{
                    Text("Favorite Shows").font(.title2).foregroundColor(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1))).fontWeight(.bold)
                    Spacer()
                }
                ScrollView(.horizontal){
                    LazyHStack(spacing: 32){
                        ForEach(viewModel.favs, id: \.self, content: { fav in
                            TVShowCard(tvShow: fav)
                        })
                    }
                }
            }
        }.padding().background(Color(#colorLiteral(red: 0.03921568627, green: 0.08235294118, blue: 0.1058823529, alpha: 1))).alert(isPresented: $viewModel.shouldPresentAlert, content: {
            Alert(title: Text(viewModel.alertTitle), message: Text(viewModel.alertMessage), dismissButton: .default(Text("OK")))
        })
    }
}

struct TVShowCard: View{
    @State var tvShow: TVShow
    
    var body: some View{
        VStack(spacing: 0){
            KFImage.url(URL(string: "https://image.tmdb.org/t/p/w500/\(tvShow.posterPath ?? "")")!).placeholder({
                    Image("TMDb").resizable()
                }).resizable().frame(width: 190, height: 250, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).scaledToFill()
                VStack{
                    
                    ZStack{
                        RoundedRectangle(cornerRadius: 10.0).foregroundColor(Color(#colorLiteral(red: 0.09019607843, green: 0.1272137165, blue: 0.1502157152, alpha: 1)))
                        
                        VStack{
                            HStack{
                                Text(tvShow.name).font(.body).foregroundColor(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1))).fontWeight(.bold)
                                Spacer()
                            }.padding(4)
                            
                            HStack{
                                Text(tvShow.firstAirDate).font(.caption2).foregroundColor(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1))).fontWeight(.bold)
                                Spacer()
                                Text("★  \(String(format: "%.1f", tvShow.voteAverage))") .font(.caption2).foregroundColor(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1))).fontWeight(.bold).multilineTextAlignment(.trailing)
                            }.padding(4)
                            
                            Text(tvShow.overview).foregroundColor(Color(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))).font(.caption2).multilineTextAlignment(.leading).lineLimit(4).frame(width: 180)
                        }.padding(4)
                    }
                    
                }
            
        }
        
    }
    
}


