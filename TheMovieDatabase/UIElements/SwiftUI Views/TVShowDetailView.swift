//
//  TVShowDetailView.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/18/21.
//

import SwiftUI
import Kingfisher

struct TVShowDetailView: View {
    var parent: TVShowDetailViewController?
    @ObservedObject var viewModel: TVShowDetailViewModel
    @ObservedObject var monitor = NetworkMonitor.shared
    
    @State var shouldFill = false
    var body: some View {
        GeometryReader{ g in
            ZStack{
                Color(#colorLiteral(red: 0.03921568627, green: 0.08235294118, blue: 0.1058823529, alpha: 1)).ignoresSafeArea(edges: .all)
                    ZStack{
                        VStack{
                        KFImage.url(URL(string: viewModel.getURLForBackdrop())!).placeholder({
                                    Image("TMDb").resizable()
                                }).forceTransition().resizable().frame(maxWidth: .infinity, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).scaledToFit()
                            Spacer()
                        }.ignoresSafeArea(edges: .all)
                        
                        VStack{
                            Rectangle().foregroundColor(.clear)
                                .background(LinearGradient(gradient: Gradient(colors: [Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4178105032)), .clear]), startPoint: .top, endPoint: .bottom))
                            Spacer()
                        }.ignoresSafeArea(edges: .all)
                    }
    
                    ZStack{
                        ScrollView(showsIndicators: false){
                        ZStack{
                            VStack{
                                Spacer().frame(height: 180)
                            VStack(alignment: .leading, spacing: 16){
                                Text("Summary").font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/).foregroundColor(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1)))
                                Text(viewModel.tvShow.name).font(.title2).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/).foregroundColor(Color.white)
                                Text(viewModel.tvShow.overview).lineLimit(nil).fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/).foregroundColor(Color.white)
                                Text(viewModel.getCreatedBy()).font(.caption).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/).foregroundColor(Color.white)
                                
                                Text("Last Season").font(.title2).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/).foregroundColor(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1)))
                                
                                HStack(alignment: .center, spacing: 16){
                                    
                                    KFImage.url(URL(string: viewModel.getLastSeasonPosterURL())!).placeholder({
                                        Image("TMDb").resizable().frame(width: g.size.width * 0.4, height: 250, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                    }).forceTransition().resizable().frame(width: 150, height: 250, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                              
                                    VStack(alignment: .leading, spacing: 8){
                                        Text(viewModel.getLastSeasonName()).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/).foregroundColor(Color.white)
                                        Text(viewModel.getLastSeasonDate()).font(.caption).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/).foregroundColor(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1)))
                                        Button(action: {
                        
                                            guard let details = viewModel.tvShowDetails else {return}
                                            parent?.segueFromSwiftUI(seasons: details.seasons, tvshow: viewModel.tvShow)
                                            
                                            
                                        }, label: {
                                            Text("View All Seasons").font(.subheadline).frame(minWidth: 0, maxWidth: .infinity, minHeight: 35).foregroundColor(.white).background(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1))).cornerRadius(5.0)
                                        })
                                    }.frame(width: g.size.width*0.4)
                                }
                                Text("Cast").font(.title2).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/).foregroundColor(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1)))
                                ScrollView(.horizontal){
                                    LazyHStack{
                                        ForEach(viewModel.fetchedCast, id: \.self, content: { cast in
                                            VStack{
                                                KFImage.url(URL(string: viewModel.getURLForCast(for: cast))!)
                                                    .placeholder({
                                                        Image("TMDb").resizable().scaledToFit().clipShape(Circle()).frame(width: 150, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                                    }).forceTransition().resizable().scaledToFit().clipShape(Circle()).frame(width: 90, height: 160, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                                Text(cast.name).foregroundColor(.white).frame(width: g.size.width*0.4)
                                            }
                                        })
                                    }
                                }
                            }.padding().background(Color(#colorLiteral(red: 0.1450980392, green: 0.1529411765, blue: 0.1843137255, alpha: 1))).cornerRadius(15.0)
                            }
                            
                            VStack{
                                Spacer().frame(height: 150)
                                HStack{
                                    Spacer()
                                    VStack(spacing:4){
                                        Text("\(String(format: "%.1f", viewModel.tvShow.voteAverage))").foregroundColor(.white).frame(width: 60, height: 60).background(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1))).clipShape(Circle())
                                        Button(action: {
                               
                                            shouldFill = !shouldFill
                                            viewModel.markAsFavourite()
                                        }, label: {
                                            Image(systemName: (viewModel.isFavourite) ? "heart.fill" : "heart").resizable().frame(width: 25, height: 25, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).foregroundColor(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1)))
                                        })
                                        Spacer()
                                    }
                                }.padding()
                            }
                        }
                    }.frame(maxWidth: .infinity).background(Color.clear).padding()
                        VStack{
                            Spacer()
                            if(!monitor.isConnected){
                                Text("No Internet Connection!").foregroundColor(.white).frame(width: g.size.width,height: g.size.height * 0.05).background(Color.red).ignoresSafeArea(edges: /*@START_MENU_TOKEN@*/.bottom/*@END_MENU_TOKEN@*/)
                            }
                        }
                }
            }
        }.background(Color(#colorLiteral(red: 0.09019607843, green: 0.1272137165, blue: 0.1502157152, alpha: 1))).ignoresSafeArea(.container, edges: .vertical).alert(isPresented: $viewModel.shouldPresentAlert, content: {
            Alert(title: Text(viewModel.alertTitle), message: Text(viewModel.alertMessage), dismissButton: .default(Text("OK")))
        })
    }
    
}
