//
//  EpisodeDetailView.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/20/21.
//

import SwiftUI
import Kingfisher

struct EpisodeDetailView: View {
    var parent: TVShowDetailViewController?
    @ObservedObject var viewModel: EpisodeDetailViewModel
    @ObservedObject var monitor = NetworkMonitor.shared
    
    var body: some View {
        
        GeometryReader{ g in
            ZStack{
                Color(#colorLiteral(red: 0.03921568627, green: 0.08235294118, blue: 0.1058823529, alpha: 1)).ignoresSafeArea(edges: .all)
                ZStack{
                    VStack{
                    KFImage.url(URL(string: viewModel.getURLForBackdrop())!).placeholder({
                                Image("TMDb").resizable()
                            }).forceTransition().resizable().frame(maxWidth: .infinity, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).scaledToFit()
                        Spacer()
                    }.ignoresSafeArea(edges: .all)
                    
                    VStack{
                        Rectangle().foregroundColor(.clear)
                            .background(LinearGradient(gradient: Gradient(colors: [Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4178105032)), .clear]), startPoint: .top, endPoint: .bottom))
                        Spacer()
                    }.ignoresSafeArea(edges: .all)
                }
                
                    ZStack{
                        ScrollView(showsIndicators: false){
                        ZStack{
                            VStack{
                                Spacer().frame(height: 180)
                            VStack(alignment: .leading, spacing: 16){
                                Text("Summary").font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/).foregroundColor(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1)))
                                Text(viewModel.episode.name).font(.title2).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/).foregroundColor(Color.white)
                                Text(viewModel.episode.overview).lineLimit(nil).fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/).foregroundColor(Color.white)
                                Text("Guest Stars").font(.title2).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/).foregroundColor(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1)))
                                ScrollView(.horizontal){
                                    LazyHStack{
                                        if(viewModel.fetchedGuestStars.isEmpty){
                                            Text("There are no guest stars in this episode.").font(.caption).foregroundColor(.white)
                                        }else{
                                            ForEach(viewModel.fetchedGuestStars, id: \.self, content: { star in
                                                VStack{
                                                    KFImage.url(URL(string: viewModel.getURLForGuestStars(for: star))!)
                                                        .placeholder({
                                                            Image("TMDb").resizable().scaledToFit().clipShape(Circle()).frame(width: 150, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                                        }).resizable().scaledToFit().clipShape(Circle()).frame(width: 90, height: 160, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                                    
                                                    Text(star.name).foregroundColor(.white).frame(width: g.size.width*0.4)
                                                }
                                            })
                                        }
                                    }
                                }
                            }.padding().background(Color(#colorLiteral(red: 0.1450980392, green: 0.1529411765, blue: 0.1843137255, alpha: 1))).cornerRadius(15.0)
                            }
                            
                            VStack{
                                Spacer().frame(height: 150)
                                HStack{
                                    Spacer()
                                    VStack(spacing:4){
                                        Text("\(String(format: "%.1f", viewModel.episode.voteAverage))").foregroundColor(.white).frame(width: 60, height: 60).background(Color(#colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1))).clipShape(Circle())
                                        Spacer()
                                    }
                                }.padding()
                            }
                            
                            
                            
                        }
                    }.frame(maxWidth: .infinity).background(Color.clear).padding()
                        VStack{
                            Spacer()
                            if(!monitor.isConnected){
                                Text("No Internet Connection!").foregroundColor(.white).frame(width: g.size.width,height: g.size.height * 0.05).background(Color.red).ignoresSafeArea(edges: /*@START_MENU_TOKEN@*/.bottom/*@END_MENU_TOKEN@*/).alert(isPresented: $viewModel.shouldPresentAlert, content: {
                                    Alert(title: Text(viewModel.alertTitle), message: Text(viewModel.alertMessage), dismissButton: .default(Text("OK")))
                                })

                            }
                        }
                }
            }
        }.background(Color(#colorLiteral(red: 0.09019607843, green: 0.1272137165, blue: 0.1502157152, alpha: 1))).ignoresSafeArea(.container, edges: .vertical)
    }
    
    
    
    
    
}
