//
//  TMDBButton.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/11/21.
//

import UIKit

@IBDesignable
class TMDBButton: UIButton {
    var borderWidth:CGFloat = 2.0
    var color:CGColor = #colorLiteral(red: 0.1294117647, green: 0.8039215686, blue: 0.3960784314, alpha: 1)
    
    @IBInspectable
    var titleText: String?{
        didSet{
            self.setTitle(titleText, for: .normal)
            self.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        }
    }
    override init(frame: CGRect){
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    func setup(){
        self.clipsToBounds = true
        self.layer.backgroundColor = color
        self.titleEdgeInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        self.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        self.layer.cornerRadius = 4.0
    }
}
