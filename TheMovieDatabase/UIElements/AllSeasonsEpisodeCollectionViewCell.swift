//
//  AllSeasonsEpisodeCollectionViewCell.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/20/21.
//

import UIKit

class AllSeasonsEpisodeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var episodeName: UILabel!
    @IBOutlet weak var episodeImage: UIImageView!
    
    
}
