//
//  TVShowsViewModel.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/17/21.
//

import Foundation
import Combine
import Unrealm

enum ShowListType: String{
    case popular = "popular"
    case topRated = "topRated"
    case onTV = "onTV"
    case airingToday = "airingToday"
}

class TVShowsViewModel {
    private let service: Service
    var delegate: TVShowsViewModelDelegate?
    var fetchedShows: [TVShow] = []
    var currentShowListType: ShowListType = .popular
    var currentPage = 1
    var maxPage = 100
    private var vmConfig: ViewModelConfiguration
    
    private var cancellables: Set<AnyCancellable> = []
    
    init(service: Service = APIService(), vmConfig: ViewModelConfiguration = .normal){
        self.service = service
        self.vmConfig = vmConfig
    }
    
    func fetchTVShowList(){
        sinkDataFromViewModel()
        
    }
    
    func changeListType(to type: ShowListType){
        currentShowListType = type
        emptyShows()
        fetchTVShowList()
    }
    
    func fetchNextPage(){
        if(currentPage < maxPage){
            currentPage += 1
        }else{
            return
        }
        sinkDataFromViewModel()
    }
    
    func emptyShows(){
        fetchedShows = []
        currentPage = 1
        delegate?.fetchSuccessful(with: fetchedShows)
    }
    
    func sinkDataFromViewModel(){
        if(NetworkMonitor.shared.isConnected || vmConfig == .testCase){
            service.getTVShowList(of: currentShowListType, at: currentPage)
                .receive(on: DispatchQueue.main)
                .sink(receiveCompletion: { [weak self] completion in
                    guard let self = self else {return}
                    switch(completion){
                    case .failure(let error):
                        self.delegate?.fetchFailed(with: error)
                    case .finished:
                        self.delegate?.fetchSuccessful(with: self.fetchedShows)
                        
                    }
                }, receiveValue: { [weak self] response in
                    guard let self = self else {return}
                    self.fetchedShows.append(contentsOf: response.results)
                    self.currentPage = response.page
                    self.maxPage = response.totalPages
                    var itemToSave = response
                    itemToSave.cacheKey = "\(self.currentShowListType.rawValue)-\(self.currentPage)"
                    try! RealmManager.shared.saveIntoRealm(item: itemToSave)
                }).store(in: &cancellables)
        }else{
            if let realmShows = RealmManager.shared.getTVShowListFromRealm(with: "\(self.currentShowListType.rawValue)-\(self.currentPage)"){
                self.fetchedShows.append(contentsOf: realmShows.results)
                self.currentPage = realmShows.page
                self.maxPage = realmShows.totalPages
                delegate?.fetchSuccessful(with: self.fetchedShows)
            }else{
                delegate?.realmFetchfailed()
            }
        }
    }
}
