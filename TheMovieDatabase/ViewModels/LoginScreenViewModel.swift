//
//  LoginScreenViewModel.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/11/21.
//

import Foundation
import Combine

class LoginScreenViewModel {
    private let service: Service
    var delegate: LoginScreenViewModelDelegate?
    private var cancellables: Set<AnyCancellable> = []
    
    init(service: Service = APIService()){
        self.service = service
    }
    
    func requestRemoteLogin(){
        service.getRequestToken().receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                guard let self = self else {return}
                switch(completion){
                case .failure(let error):
                    self.delegate?.remoteLoginFailed(with: error)
                default:
                    break
                }
                
            }, receiveValue: { [weak self] token in
                guard let self = self else {return}
                self.delegate?.remoteLoginSuccessful(with: token)
            }).store(in: &cancellables)
    }
}
