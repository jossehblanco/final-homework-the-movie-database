//
//  TVShowDetailViewModel.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/19/21.
//

import Foundation
import Combine

class TVShowDetailViewModel: ObservableObject, AlertingViewModel{
    private let service: Service
    @Published var tvShow: TVShow
    @Published var tvShowDetails: TVShowDetail?
    @Published var fetchedCast: [Cast] = []
    @Published var backDrop: String = ""
    @Published var accountDetails = Account()
    @Published var favs: [TVShow] = []
    @Published var alertTitle: String = ""
    @Published var alertMessage: String = ""
    @Published var shouldPresentAlert: Bool = false
    @Published var isFavourite = false
    
    private var cancellables: Set<AnyCancellable> = []
    private var vmConfig: ViewModelConfiguration
    
    init(service: Service = APIService(), tvShow: TVShow, vmConfig: ViewModelConfiguration = .normal){
        self.service = service
        self.tvShow = tvShow
        self.vmConfig = vmConfig
        fetchTVShowDetails()
        fetchTVShowCredits()
        fetchFavs()
    }
    
    func fetchTVShowDetails(){
        
        if(NetworkMonitor.shared.isConnected || vmConfig == .testCase){
            service.getTVShowDetails(for: tvShow.id).receive(on: DispatchQueue.main).sink(receiveCompletion: { completion in
                switch(completion){
                case .failure(let error):
                    self.enqueueAlert(withTitle: "Error", withMessage: "Couldnt Fetch Data.")
                case .finished:
                    break
                }
            }, receiveValue: { [weak self] tvShowDetails in
                guard let self = self else {return}
                self.tvShowDetails = tvShowDetails
                var itemToSave = tvShowDetails
                itemToSave.cacheKey = "\(self.tvShow.id)-details"
                try! RealmManager.shared.saveIntoRealm(item: itemToSave)
            }).store(in: &cancellables)
        }else{
            if let realmDetails = RealmManager.shared.getTVShowDetails(with: "\(self.tvShow.id)-details") {
                self.tvShowDetails = realmDetails
            }else{
                enqueueAlert(withTitle: "No Data", withMessage: "Failed to load from local resource. Please connect to the Internet.")
            }
        }
    }
    
    func fetchTVShowCredits(){
        if(NetworkMonitor.shared.isConnected || vmConfig == .testCase){
            service.getTVShowCredits(for: tvShow.id).receive(on: DispatchQueue.main).sink(receiveCompletion: { completion in
                switch(completion){
                case .failure(let error):
                    self.enqueueAlert(withTitle: "Error", withMessage: "Couldn't Fetch Data.")
                case .finished:
                    break
                }
            }, receiveValue: { [weak self] tvShowCredits in
                guard let self = self else {return}
                self.fetchedCast = tvShowCredits.cast
                var credits = tvShowCredits
                credits.cacheKey = "\(self.tvShow.id)-credits"
                try! RealmManager.shared.saveIntoRealm(item: credits)
                
            }).store(in: &cancellables)
        }else{
            if let realmCredits = RealmManager.shared.getTVShowCredits(with: "\(self.tvShow.id)-credits") {
                self.fetchedCast = realmCredits.cast
            }else{
                enqueueAlert(withTitle: "No Data", withMessage: "Failed to load from local resource. Please connect to the Internet.")
            }
        }
    }
    
    func getCreatedBy() -> String{
        var credits = "Created by: "
        guard let authors = tvShowDetails?.createdBy else {return "No Author Information Available"}
        for authorIndex in 0..<authors.count{
            if(authorIndex < authors.count - 2){
                credits += authors[authorIndex].name + ", "
            }else{
                credits += authors[authorIndex].name
            }
        }
        return credits
    }
    
    func getLastSeasonName()-> String{
        tvShowDetails?.seasons.last?.name ?? "Last Season"
    }
    
    func getLastSeasonDate()-> String{
        tvShowDetails?.seasons.last?.airDate ?? "Last Season Air Date"
    }
    
    func getLastSeasonPosterURL()->String{
        "https://image.tmdb.org/t/p/w500/\(tvShowDetails?.seasons.last?.posterPath ?? "")"
    }
    
    func updateBackdrop(){
        backDrop = "https://image.tmdb.org/t/p/w500/\(tvShow.backdropPath ?? "")"
    }
    
    func getURLForBackdrop() -> String{
        return "https://image.tmdb.org/t/p/w500/\(tvShow.backdropPath ?? "")"
    }
    
    func getURLForCast(for cast: Cast) -> String{
        return "https://image.tmdb.org/t/p/w500/\(cast.profilePath ?? "")"
    }
    
    func markAsFavourite(){
        self.isFavourite = !self.isFavourite
        service.getAccount().receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch(completion){
                case .failure(let error):
                    self.enqueueAlert(withTitle: "Error", withMessage: "Unable to get account information")
                case .finished:
                    break
                }
                
            }, receiveValue: { account in
                self.service.markAsFavourite(tvID: self.tvShow.id, favStatus: self.isFavourite, accountID: account.id).receive(on: DispatchQueue.main).sink(receiveCompletion: { completion in
                    switch(completion){
                    case .failure(let error):
                        self.enqueueAlert(withTitle: "Error", withMessage: "Unable to mark as favourite")
                    case .finished:
                        break
                    }
                    
                }, receiveValue: { response in
                    self.fetchFavs()
                }).store(in: &self.cancellables)
            }).store(in: &cancellables)
    }
    
    
    
    func fetchFavs(){
        service.getAccount().receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch(completion){
                case .failure(let error):
                    self.enqueueAlert(withTitle: "Error", withMessage: "Failed to load account information")
                case .finished:
                    break
                }
            }, receiveValue: { account in
                self.service.getFavs(account: account.id).receive(on: DispatchQueue.main).sink(receiveCompletion: { completion in
                    switch(completion){
                    case .failure(let error):
                        self.enqueueAlert(withTitle: "Error", withMessage: "Could Not Retrieve Favourite Shows")
                    case .finished:
                        break
                    }
                }, receiveValue: { favourites in
                    self.favs = favourites.results
                    self.accountDetails = account
                    self.isFavourite = self.favs.contains(where: { $0.id == self.tvShow.id
                    })
                }).store(in: &self.cancellables)
                //GetFavourites
            }).store(in: &cancellables)
    }
    
}
