//
//  ViewModelConfiguration.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/21/21.
//

import Foundation

enum ViewModelConfiguration{
    case testCase
    case normal
}
