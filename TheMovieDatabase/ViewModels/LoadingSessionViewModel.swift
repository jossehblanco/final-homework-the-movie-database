//
//  LoadingSessionViewModel.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/14/21.
//

import Foundation
import Combine

class LoadingSessionViewModel{
    
    private let service: Service
    var request_token:RequestToken?
    var delegate: LoadingSessionViewModelDelegate?
    private var cancellables: Set<AnyCancellable> = []
    
    init(service: Service = APIService()){
        self.service = service
    }
    
    func createLoginSession(){
        service.getSessionId(with: request_token?.request_token ?? "" ).receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                guard let self = self else {return}
                
                switch(completion){
                case . failure(let error):
                    self.delegate?.sessionFailed(with: error)
                default:
                    break
                }
                
            }, receiveValue: { [weak self] session in
                guard let self = self else {return}
                self.delegate?.sessionCreated(with: session)
            }).store(in: &cancellables)
    }
    
}
