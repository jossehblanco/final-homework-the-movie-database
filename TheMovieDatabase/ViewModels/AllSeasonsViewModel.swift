//
//  AllSeasonsViewModel.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/20/21.
//

import Foundation
import Combine

class AllSeasonsViewModel{
    private let service: Service
    var delegate: AllSeasonsViewModelDelegate?
    var seasons: [Season]
    var tvShow: TVShow
    var seasonDetails: [SeasonDetail] = []
    private var vmConfig: ViewModelConfiguration
    
    private var cancellables: Set<AnyCancellable> = []
    
    init(service: Service = APIService(), tvShow: TVShow, seasons: [Season], vmConfig: ViewModelConfiguration = .normal){
        self.service = service
        self.tvShow = tvShow
        self.seasons = seasons
        self.vmConfig = vmConfig
        fetchSeasonDetails()
    }
    
    func fetchSeasonDetails(){
        
        if(NetworkMonitor.shared.isConnected || vmConfig == .testCase){
            var seasonDetailPublishers: [AnyPublisher<SeasonDetail, Error>] = []
            for seasonNumber in seasons.map({$0.seasonNumber}){
                seasonDetailPublishers.append(service.getSeasonDetails(of: seasonNumber, for: tvShow.id))
            }
            let details = Publishers.MergeMany(seasonDetailPublishers).collect().eraseToAnyPublisher()
            
            details.receive(on: DispatchQueue.main).sink(receiveCompletion: { completion in
                switch(completion){
                case .failure(let error):
                    self.delegate?.fetchFailed()
                case .finished:
                    self.delegate?.fetchSuccessful()
                }
            }, receiveValue: { result in
                self.seasonDetails = result.sorted(by: { $0.seasonNumber > $1.seasonNumber})
                var details = RealmSeasonDetails(seasonDetails: result)
                details.cacheKey = "\(self.tvShow.id)-seasonDetails"
                try! RealmManager.shared.saveIntoRealm(item: details)
            }).store(in: &cancellables)
        }else{
            if let realmDetails = RealmManager.shared.getSeasonDetails(with: "\(self.tvShow.id)-seasonDetails") {
                self.seasonDetails = realmDetails.seasonDetails
            }else{
                delegate?.realmFetchFailed()
            }
            
        }
    }
    
    func getURLForEpisode(for episode: Episode) -> String{
        return "https://image.tmdb.org/t/p/w500/\(episode.stillPath ?? "")"
    }
    
}
