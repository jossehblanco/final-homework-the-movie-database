//
//  FavouriteScreenViewModel.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/21/21.
//

import Foundation
import Combine

class FavouriteScreenViewModel: ObservableObject, AlertingViewModel{
    private let service: Service
    @Published var accountDetails = Account()
    @Published var favs: [TVShow] = []
    @Published var alertTitle: String = ""
    @Published var alertMessage: String = ""
    @Published var shouldPresentAlert: Bool = false

    private var cancellables: Set<AnyCancellable> = []
    private var vmConfig: ViewModelConfiguration
    
    
    init(service: Service = APIService(), vmConfig: ViewModelConfiguration = .normal){
        self.service = service
        self.vmConfig = vmConfig
        fetchAccount()
    }
    
    func fetchAccount(){
        service.getAccount().receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch(completion){
                case .failure(let error):
                    self.enqueueAlert(withTitle: "Error", withMessage: "Failed to load account information")
                case .finished:
                    break
                }
            }, receiveValue: { account in
                self.service.getFavs(account: account.id).receive(on: DispatchQueue.main).sink(receiveCompletion: { completion in
                    switch(completion){
                    case .failure(let error):
                        self.enqueueAlert(withTitle: "Error", withMessage: "Could Not Retrieve Favourite Shows")
                    case .finished:
                        break
                    }
                }, receiveValue: { favourites in
                    self.favs = favourites.results
                    self.accountDetails = account
                }).store(in: &self.cancellables)
                //GetFavourites
            }).store(in: &cancellables)
    }
    
    func getURLForAvatar() -> String{
        return "https://image.tmdb.org/t/p/w500/\(accountDetails.avatar.tmdb?.avatar_path ?? "")"
    }
    
}
