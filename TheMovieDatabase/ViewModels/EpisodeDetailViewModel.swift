//
//  EpisodeDetailViewModel.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/20/21.
//

import Foundation
import Combine

class EpisodeDetailViewModel: ObservableObject, AlertingViewModel{
    private let service: Service
    @Published var tvShow: TVShow
    @Published var episode: Episode
    @Published var episodeDetails: EpisodeDetail?
    @Published var fetchedGuestStars: [GuestStar] = []
    @Published var alertTitle: String = ""
    @Published var alertMessage: String = ""
    @Published var shouldPresentAlert: Bool = false

    private var cancellables: Set<AnyCancellable> = []
    private var vmConfig: ViewModelConfiguration
    
    init(service: Service = APIService(), tvShow: TVShow, episode: Episode, vmConfig: ViewModelConfiguration = .normal){
        self.service = service
        self.tvShow = tvShow
        self.episode = episode
        self.vmConfig = vmConfig
        fetchEpisodeDetails()
    }
    
    func fetchEpisodeDetails(){
        if(NetworkMonitor.shared.isConnected || vmConfig == .testCase){
            service.getEpisodeDetails(tvID: tvShow.id, seasonNumber: episode.seasonNumber, episodeNumber: episode.episodeNumber).receive(on: DispatchQueue.main).sink(receiveCompletion: { completion in
                switch(completion){
                case .failure(let error):
                    self.enqueueAlert(withTitle: "Error", withMessage: "Unable to get Episode Information")
                    
                case .finished:
                    break
                }
            }, receiveValue: { episodeDetail in
                self.episodeDetails = episodeDetail
                guard let details = self.episodeDetails else {return}
                self.fetchedGuestStars = details.guestStars
                var itemToSave = details
                itemToSave.cacheKey = "\(self.tvShow.id)-\(self.episode.seasonNumber)-\(self.episode.episodeNumber)"
                try! RealmManager.shared.saveIntoRealm(item: itemToSave)
                
            }).store(in: &cancellables)
        }else{
            if let realmDetails = RealmManager.shared.getEpisodeDetails(with: "\(self.tvShow.id)-\(self.episode.seasonNumber)-\(self.episode.episodeNumber)") {
                self.episodeDetails = realmDetails
                self.fetchedGuestStars = realmDetails.guestStars
            }else{
                enqueueAlert(withTitle: "No Data", withMessage: "Failed to load from local resource. Please connect to the Internet.")
                
            }
        }
    }
    
    

    func getURLForBackdrop() -> String{
        return "https://image.tmdb.org/t/p/w500/\(episode.stillPath ?? "")"
   }
//
    func getURLForGuestStars(for stars: GuestStar) -> String{
        return "https://image.tmdb.org/t/p/w500/\(stars.profilePath ?? "")"
    }
    
}
