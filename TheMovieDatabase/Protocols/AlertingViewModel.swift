//
//  AlertingViewModel.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/21/21.
//

import Foundation
import Combine
import SwiftUI

//MARK: AlertingViewModel protocol
protocol AlertingViewModel: AnyObject{
    var alertTitle: String {get set}
    var alertMessage: String {get set}
    var shouldPresentAlert: Bool { get set }
    func enqueueAlert(withTitle title: String, withMessage message: String)
}

//MARK: AlertingViewModel Default Implementation
extension AlertingViewModel{
    func enqueueAlert(withTitle title: String, withMessage message: String) {
        self.alertMessage = message
        self.alertTitle = title
        self.shouldPresentAlert = true
    }
}
