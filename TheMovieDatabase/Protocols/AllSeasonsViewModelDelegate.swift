//
//  AllSeasonsViewModelDelegate.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/20/21.
//

import Foundation

protocol AllSeasonsViewModelDelegate{
    func fetchSuccessful()
    func fetchFailed()
    func realmFetchFailed()
}
