//
//  Requester.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/11/21.
//

import Foundation
import Combine

//MARK: Defines the Requester protocol
protocol Requester{
    func requestData(from url: String) -> AnyPublisher<Data, Error>
    func postRequest(to url: String, with parameters: [String: Any]) -> AnyPublisher<Data, Error>
}
