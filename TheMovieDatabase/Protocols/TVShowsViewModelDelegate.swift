//
//  TVShowsViewModelDelegate.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/17/21.
//

import Foundation

protocol TVShowsViewModelDelegate{
    func fetchSuccessful(with tvShowlist: [TVShow])
    func fetchFailed(with error: Error)
    func realmFetchfailed()
}
