//
//  URLSessionExecuter.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/15/21.
//

import Foundation
import Combine

class URLSessionExecuter {
    private var session: URLSession = URLSession.shared
    
    func execute(for url: String) -> AnyPublisher<Data, Error> {
        let stringAsURL = URL(string: url)
        let request = URLRequest(url: stringAsURL!)
        
        return session.dataTaskPublisher(for: request).tryMap({
            element -> Data in
            guard let httpResponse = element.response as? HTTPURLResponse,
                  httpResponse.statusCode == 200 else{
                throw URLError(.badServerResponse)
            }
            let response = CachedURLResponse(response: httpResponse, data: element.data)
            URLCache.shared.storeCachedResponse(response, for: request)
            return element.data
    }).eraseToAnyPublisher()
    }
    
    func executePost(for url: String, parameters: [String:Any]) -> AnyPublisher<Data, Error> {
        let stringAsURL = URL(string: url)
        var request = URLRequest(url: stringAsURL!)
        request.httpMethod = "POST"
        do{
           let body = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            request.httpBody = body
        }catch let error{
            print(error)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        request.addValue("Accept", forHTTPHeaderField: "Content-type")
        
        return session.dataTaskPublisher(for: request).tryMap({
            element -> Data in
            guard let httpResponse = element.response as? HTTPURLResponse,
                  httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else{
                throw URLError(.badServerResponse)
            }
            return element.data
        }).eraseToAnyPublisher()
    }
}

