//
//  LoadingSessionViewModelDelegate.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/16/21.
//

import Foundation

protocol LoadingSessionViewModelDelegate{
    func sessionCreated(with session: Session)
    func sessionFailed(with error: Error)
}
