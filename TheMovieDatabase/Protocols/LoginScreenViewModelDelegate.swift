//
//  LoadingScreenViewModelDelegate.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/16/21.
//

import Foundation

protocol LoginScreenViewModelDelegate{
    func remoteLoginSuccessful(with token: RequestToken)
    func remoteLoginFailed(with error: Error)
}

