//
//  Service.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/11/21.
//

import Foundation
import Combine

protocol Service{
    
    func getRequestToken()->AnyPublisher<RequestToken, Error>
    func getSessionId(with request: String)->AnyPublisher<Session, Error>
    func append(APIKey: String, to url: String)->String
    func getTVShowList(of type: ShowListType, at page: Int)-> AnyPublisher<TVShowListResponse, Error>
    func getTVShowDetails(for id: Int) -> AnyPublisher<TVShowDetail, Error>
    func getTVShowCredits(for id: Int) -> AnyPublisher<Credit, Error>
    func getSeasonDetails(of season: Int, for tvShow: Int) -> AnyPublisher<SeasonDetail, Error>
    func getEpisodeDetails(tvID: Int, seasonNumber: Int, episodeNumber: Int) -> AnyPublisher<EpisodeDetail, Error>
    func getAccount() -> AnyPublisher<Account, Error>
    func markAsFavourite(tvID: Int, favStatus: Bool, accountID: Int ) -> AnyPublisher<MarkAsFavouriteResponse, Error>
    func getFavs(account: Int) -> AnyPublisher<TVShowListResponse, Error>
}
