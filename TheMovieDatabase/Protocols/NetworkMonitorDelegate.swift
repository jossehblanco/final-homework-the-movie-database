//
//  NetworkMonitorDelegate.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/21/21.
//

import Foundation

protocol NetworkMonitorDelegate{
    func connectionDidChange(connected: Bool)
}
