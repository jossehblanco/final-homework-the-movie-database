//
//  NetworkRequester.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/11/21.
//

import Foundation
import Combine

class NetworkRequester: Requester {
    private var urlSession : URLSessionExecuter //URLSession
    
    //Receives URLSession by injection
    init(urlSession : URLSessionExecuter = URLSessionExecuter()){
        self.urlSession = urlSession
    }
    
    //Executes the execute method given the configuration
    func requestData(from url: String) -> AnyPublisher<Data, Error>{
        return urlSession.execute(for: url)//execute
    }
    func postRequest(to url: String, with parameters: [String: Any]) -> AnyPublisher<Data, Error> {
        return urlSession.executePost(for: url, parameters: parameters)
    }
}
