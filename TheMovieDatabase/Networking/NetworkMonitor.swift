//
//  NetworkMonitor.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/21/21.
//

import Foundation
import Network

//This class monitors internet Reachability.
final class NetworkMonitor: ObservableObject{
    static let shared = NetworkMonitor()
    
    private let queue = DispatchQueue.main
    
    private let monitor: NWPathMonitor
    private var starting = true
    //Use NeworkMonitor.shared.isConected to evaluate internet connection.
    public private(set) var isConnected: Bool = InternetConnectionManager.isConnectedToNetwork()
    public private(set) var connectionType: ConnectionType?
    
    var delegate: NetworkMonitorDelegate?
    
    enum ConnectionType {
        case wifi
        case cellular
        case ethernet
        case unknown
    }
    
    private init(){
        monitor = NWPathMonitor()
    }
    
    public func startMonitor(){
        monitor.start(queue: queue)
        monitor.pathUpdateHandler = {[weak self] path in
            guard let self = self else {return}
            if(self.starting){
                self.isConnected = InternetConnectionManager.isConnectedToNetwork()
                self.getConnectionType(path)
                self.starting = false
            }else{
                let _ = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { timer in
                    self.isConnected = InternetConnectionManager.isConnectedToNetwork()
                    self.getConnectionType(path)
                    DispatchQueue.main.async {
                        self.delegate?.connectionDidChange(connected: self.isConnected)
                    }
                }
            }
            
        }
    }
    
    private func getConnectionType(_ path: NWPath){
        if path.usesInterfaceType(.wifi) {
            connectionType = .wifi
        } else if path.usesInterfaceType(.cellular){
            connectionType = .cellular
        } else if path.usesInterfaceType(.wiredEthernet){
            connectionType = .ethernet
        } else {
            connectionType = .unknown
        }
    }
    
    public func stopMonitoring(){
        monitor.cancel()
    }
}
