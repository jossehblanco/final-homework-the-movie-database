//
//  TMDBService.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/11/21.
//

import Foundation
import Combine
import KeychainAccess

enum Routes:String{
    case requestToken = "/authentication/token/new"
    case createSession = "/authentication/session/new"
    case tvPopular = "/tv/popular"
    case tvTopRated = "/tv/top_rated"
    case tvAiringToday = "/tv/airing_today"
    case tvOnTV = "/tv/on_the_air"
    case tvShowDetails = "/tv/"
    case tvShowCredits = "/tv/id/credits"
    case seasonDetails = "/tv/id/season/number"
    case episodeDetails = "/tv/id/season/number/episode/epnum"
    case account = "/account"
    case markAsFav = "/account/idacc/favorite"
    case getFavs = "/account/idacc/favorite/tv"
}

class APIService: Service{
    
    private let baseURL = "https://api.themoviedb.org/3"
    private var networkRequester:Requester
    private let APIKey: String = "a869a420f9f3eb7979d545147b430d2c"
    
    init(networkRequester: Requester = NetworkRequester()){
        self.networkRequester = networkRequester
    }
    
    func getRequestToken() -> AnyPublisher<RequestToken, Error> {
        let url = append(APIKey: APIKey, to: baseURL+Routes.requestToken.rawValue)
        return networkRequester.requestData(from: url).decode(type: RequestToken.self, decoder: JSONDecoder()).eraseToAnyPublisher()
    }
    
    func append(APIKey: String, to url: String) -> String {
        return url+"?api_key="+APIKey
    }
    
    func getSessionId(with request: String) -> AnyPublisher<Session, Error> {
        let parameters = ["request_token" : request]
        return networkRequester.postRequest(to: append(APIKey: APIKey, to: baseURL + Routes.createSession.rawValue), with: parameters)
            .decode(type: Session.self, decoder: JSONDecoder()).eraseToAnyPublisher()
    }
    
    func getTVShowList(of type: ShowListType, at page: Int)-> AnyPublisher<TVShowListResponse, Error>{
        var url = ""
        switch(type){
        case .popular:
            url = append(APIKey: APIKey, to: baseURL+Routes.tvPopular.rawValue).appending("&page=\(page)")
        case .topRated:
            url = append(APIKey: APIKey, to: baseURL+Routes.tvTopRated.rawValue).appending("&page=\(page)")
        case .onTV:
            url = append(APIKey: APIKey, to: baseURL+Routes.tvOnTV.rawValue).appending("&page=\(page)")
        case .airingToday:
            url = append(APIKey: APIKey, to: baseURL+Routes.tvAiringToday.rawValue).appending("&page=\(page)")
        }
        return networkRequester.requestData(from: url).decode(type: TVShowListResponse.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    func getTVShowDetails(for id: Int) -> AnyPublisher<TVShowDetail, Error>{
        let url = append(APIKey: APIKey, to: baseURL + Routes.tvShowDetails.rawValue + "\(id)")
        return networkRequester.requestData(from: url).decode(type: TVShowDetail.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    
    func getTVShowCredits(for id: Int) -> AnyPublisher<Credit, Error>{
        let url = append(APIKey: APIKey, to: baseURL + Routes.tvShowCredits.rawValue.replacingOccurrences(of: "id", with: "\(id)"))
        return networkRequester.requestData(from: url).decode(type: Credit.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    
    func getSeasonDetails(of season: Int, for tvShow: Int) -> AnyPublisher<SeasonDetail, Error>{
        let url = append(APIKey: APIKey, to: baseURL + Routes.seasonDetails.rawValue.replacingOccurrences(of: "id", with: "\(tvShow)").replacingOccurrences(of: "number", with: "\(season)"))
        
        return networkRequester.requestData(from: url).decode(type: SeasonDetail.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    func getEpisodeDetails(tvID: Int, seasonNumber: Int, episodeNumber: Int) -> AnyPublisher<EpisodeDetail, Error>{
        let url = append(APIKey: APIKey, to: baseURL + Routes.episodeDetails.rawValue.replacingOccurrences(of: "id", with: "\(tvID)").replacingOccurrences(of: "number", with: "\(seasonNumber)").replacingOccurrences(of: "epnum", with: "\(episodeNumber)"))
        
        return networkRequester.requestData(from: url).decode(type: EpisodeDetail.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
   
    //MARK: - UNTESTED
    
    func getAccount() -> AnyPublisher<Account, Error>{
        let keychain = Keychain(service: "com.applaudostudios.TheMovieDatabase")
        let session = keychain["session_id"]
        let url = append(APIKey: APIKey, to: baseURL + Routes.account.rawValue).appending("&session_id=\(session ?? "")")
        return networkRequester.requestData(from: url).decode(type: Account.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    func markAsFavourite(tvID: Int, favStatus: Bool, accountID: Int ) -> AnyPublisher<MarkAsFavouriteResponse, Error> {
        let keychain = Keychain(service: "com.applaudostudios.TheMovieDatabase")
        let session = keychain["session_id"]
        let parameters = ["media_type" : "tv", "media_id": tvID, "favorite" : favStatus] as [String : Any]
        let url = append(APIKey: APIKey, to: (baseURL + Routes.markAsFav.rawValue)).appending("&session_id=\(session ?? "")").replacingOccurrences(of: "idacc", with: "\(accountID)")
        return networkRequester.postRequest(to: url, with: parameters)
            .decode(type: MarkAsFavouriteResponse.self, decoder: JSONDecoder()).eraseToAnyPublisher()
    }
    
    func getFavs(account: Int) -> AnyPublisher<TVShowListResponse, Error>{
        let keychain = Keychain(service: "com.applaudostudios.TheMovieDatabase")
        let session = keychain["session_id"]
        let url = append(APIKey: APIKey, to: baseURL + Routes.getFavs.rawValue).replacingOccurrences(of: "idacc", with: "\(account)").appending("&session_id=\(session ?? "")")
        return networkRequester.requestData(from: url).decode(type: TVShowListResponse.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
