//
//  RealmManager.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/21/21.
//

import Foundation
import Unrealm

class RealmManager{
    lazy var realm = try! Realm()
    static var shared = RealmManager()
    
    
    func saveIntoRealm<T: Realmable>(item: T) throws{
        try realm.write({
            realm.add(item, update: .all)
        })
    }
    
    
    func getTVShowListFromRealm(with key: String)-> TVShowListResponse?{
        let tvShowListResponse = realm.objects(TVShowListResponse.self).filter({ tvShowList in
            tvShowList.cacheKey  == key
        }).first
        return tvShowListResponse
    }
    
    func getTVShowDetails(with key: String)-> TVShowDetail?{
        let tvShowDetail = realm.objects(TVShowDetail.self).filter({ tvShowDetail in
            tvShowDetail.cacheKey  == key
        }).first
        return tvShowDetail
    }
    
    func getTVShowCredits(with key: String)-> Credit?{
        let tvShowCredits = realm.objects(Credit.self).filter({ tvShowDetail in
            tvShowDetail.cacheKey  == key
        }).first
        return tvShowCredits
    }
    
    func getSeasonDetails(with key: String)-> RealmSeasonDetails?{
        let details = realm.objects(RealmSeasonDetails.self).filter({ seasonDetails in
            seasonDetails.cacheKey  == key
        }).first
        return details
    }
    
    func getEpisodeDetails(with key: String)-> EpisodeDetail?{
        let details = realm.objects(EpisodeDetail.self).filter({ episodeDetails in
            episodeDetails.cacheKey  == key
        }).first
        return details
        
    }
    
}
