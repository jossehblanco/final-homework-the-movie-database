//
//  AppDelegate.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/9/21.
//

import UIKit
import Unrealm
import RealmSwift
import KeychainAccess

@main
class AppDelegate: UIResponder, UIApplicationDelegate{

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
            
        Realm.registerRealmables(RequestToken.self)
        Realm.registerRealmables(Session.self)
        Realm.registerRealmables(SessionRequestBody.self)
        Realm.registerRealmables(TVShowListResponse.self)
        Realm.registerRealmables(TVShow.self)
        Realm.registerRealmables(TVShowDetail.self)
        Realm.registerRealmables(CreatedBy.self)
        Realm.registerRealmables(Genre.self)
        Realm.registerRealmables(LastEpisodeToAir.self)
        Realm.registerRealmables(Network.self)
        Realm.registerRealmables(ProductionCountry.self)
        Realm.registerRealmables(Season.self)
        Realm.registerRealmables(SpokenLanguage.self)
        Realm.registerRealmables(Cast.self)
        Realm.registerRealmables(Crew.self)
        Realm.registerRealmables(Credit.self)
        Realm.registerRealmables(SeasonDetail.self)
        Realm.registerRealmables(Episode.self)
        Realm.registerRealmables(EpisodeDetail.self)
        Realm.registerRealmables(GuestStar.self)
        Realm.registerRealmables(RealmSeasonDetails.self)
        
        NetworkMonitor.shared.startMonitor()
        
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}
