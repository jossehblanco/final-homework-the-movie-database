//
//  SceneDelegate.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/9/21.
//

import UIKit
import KeychainAccess
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        let keychain = Keychain(service: "com.applaudostudios.TheMovieDatabase")
        if let session = keychain["session_id"] {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let navigationController = storyboard.instantiateInitialViewController() as? UINavigationController,
               let rootViewController = storyboard.instantiateViewController(withIdentifier: "tvShowList") as? TVShowListViewController{
                navigationController.viewControllers = [rootViewController]
                self.window?.rootViewController = navigationController
            }

        }
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let nav = self.window?.rootViewController as? UINavigationController
        guard let existingLoginScreen = nav?.viewControllers.first(where: {$0 is LoginScreenViewController}) as? LoginScreenViewController else {return}
        if let _ = URLContexts.first?.url.valueOf("approved"){
            guard let loadingSessionVC = storyboard.instantiateViewController(identifier: "loadingSession") as? LoadingSessionViewController else {return}
            existingLoginScreen.lastSFVC?.dismiss(animated: true, completion: nil)
            guard let request_token = existingLoginScreen.request_token else {return}
            loadingSessionVC.setRequestToken(to: request_token)
            nav?.pushViewController(loadingSessionVC, animated: true)
            
        }else if let _ = URLContexts.first?.url.valueOf("denied"){
            existingLoginScreen.lastSFVC?.dismiss(animated: true, completion: nil)
        }
    }
}

extension URL {
    func valueOf(_ queryParamaterName: String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParamaterName })?.value
    }
}

