//
//  ViewController.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/9/21.
//

import UIKit
import Combine
import SafariServices
import Network

class LoginScreenViewController: UIViewController, SFSafariViewControllerDelegate, LoginScreenViewModelDelegate, NetworkMonitorDelegate{
    
    private var loginScreenVM = LoginScreenViewModel()
    private var cancellables: Set<AnyCancellable> = []
    var request_token:RequestToken?
    var lastSFVC: SFSafariViewController?
    @IBOutlet weak var loginButton: TMDBButton!
    @IBOutlet weak var connectionWarning: UILabel!
    
    let mon = NWPathMonitor()
    var queue = DispatchQueue.main
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginScreenVM.delegate = self
        NetworkMonitor.shared.delegate = self
        if(NetworkMonitor.shared.isConnected == false){
            connectionDidChange(connected: false)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
   override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }


    @IBAction func executeLogin(_ sender: Any) {
        DispatchQueue.main.async {
            self.loginScreenVM.requestRemoteLogin()
        }
    }
    
    func showSafariVC(with url: String) {
        if let urlToShow = URL(string: url) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: urlToShow, configuration: config)
            lastSFVC = vc
            present(vc, animated: true, completion: nil)
    
        } else {
            presentAlert(title: "Error", message: "Couldn't Initiate Login Window")
        }
    }
    
    
    func remoteLoginSuccessful(with token: RequestToken) {
        self.request_token = token
        self.showSafariVC(with: "https://www.themoviedb.org/authenticate/\(token.request_token)?redirect_to=tmdbapp://login")
    }
    
    func remoteLoginFailed(with error: Error) {
        presentAlert(title: "Error", message: "Unable to start login request.")
    }
    
    func connectionDidChange(connected: Bool) {
        DispatchQueue.main.async {
            switch NetworkMonitor.shared.isConnected {
            case false:
                self.connectionWarning.text = "No Internet Connection!"
                self.connectionWarning.backgroundColor = .systemRed
                self.connectionWarning.fadeIn()
                self.loginButton.isEnabled = false
            case true:
                UIView.animate(withDuration: 0.5, animations: {
                    self.connectionWarning.text = "Connected!"
                    self.connectionWarning.backgroundColor = #colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1)
                }, completion: { completion in
                    self.connectionWarning.fadeOut(delay: 0.5)
                    self.loginButton.isEnabled = true
                })
            }
        }
    }

}

