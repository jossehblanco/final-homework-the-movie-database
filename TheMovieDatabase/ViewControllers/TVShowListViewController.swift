//
//  TVShowListViewController.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/11/21.
//

import UIKit
import Kingfisher
import KeychainAccess

enum Section{
    case first
}

protocol RightBarButtonable{
    func favouritesTapped()
    func addFavouritesButton()
}

extension UIViewController: RightBarButtonable{
    @objc func favouritesTapped() {
        presentLogoutButton()
    }
    
    func addFavouritesButton() {
        let button = UIBarButtonItem(image: UIImage(systemName: "list.bullet"), style: .plain, target: self, action: #selector(favouritesTapped))
        self.navigationItem.rightBarButtonItems = [button]
    }
    func presentAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func presentLogoutButton(){
        let alert = UIAlertController(title: "Choose What to do", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "View Favourites", style: .default, handler: {_ in
            if(NetworkMonitor.shared.isConnected){
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "favVC") as? FavouriteListViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }else{
                self.presentAlert(title: "Error", message: "You must have internet connection to access this page.")
            }
        }))
        alert.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { _ in
            self.logout()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func logout(){
        let keychain = Keychain(service: "com.applaudostudios.TheMovieDatabase")
        keychain["session_id"] = nil
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "loginScreenVC") as? LoginScreenViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}

class TVShowListViewController: UIViewController, TVShowsViewModelDelegate, UICollectionViewDelegate, NetworkMonitorDelegate{
    
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var connectionWarning: UILabel!
    @IBOutlet var warningHeight: NSLayoutConstraint!
    @IBOutlet var spaceToWarningLabel: NSLayoutConstraint!
    private var tvShowsDataSource: UICollectionViewDiffableDataSource<Section, TVShow>!
    private var tvShowListVM = TVShowsViewModel()
    private var shouldFetchNextPage = true
    private var warningFrame: CGRect = CGRect()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.collectionViewLayout = configureLayout()
        createDataSource()
        updateDataSource()
        tvShowListVM.delegate = self
        tvShowListVM.fetchTVShowList()
        warningFrame = connectionWarning.frame
        connectionDidChange(connected: NetworkMonitor.shared.isConnected)
        addFavouritesButton()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1294117647, green: 0.1725490196, blue: 0.1882352941, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        collectionView.delegate = self
        NetworkMonitor.shared.delegate = self
        connectionDidChange(connected: NetworkMonitor.shared.isConnected)
    }
    
 

    @IBAction func segmentedControlChanged(_ sender: UISegmentedControl) {
        switch(sender.selectedSegmentIndex){
        case 0:
            tvShowListVM.changeListType(to: .popular)
        case 1:
            tvShowListVM.changeListType(to: .topRated)
        case 2:
            tvShowListVM.changeListType(to: .onTV)
        case 3:
            tvShowListVM.changeListType(to: .airingToday)
        default:
            break
        }
    }
}

// MARK: - CollectionView Data Source and Layout
extension TVShowListViewController {
    
    // MARK: - Creating Data Source
    private func createDataSource() {
        tvShowsDataSource = UICollectionViewDiffableDataSource(
            collectionView: collectionView,
            cellProvider: { [weak self] (collectionView, indexPath, data) -> UICollectionViewCell? in
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tvShowListCell", for: indexPath) as! TVShowCollectionViewCell
                guard let self = self else{ return cell }
                if let data = self.tvShowsDataSource.itemIdentifier(for: indexPath){
                    cell.movieName.text = data.name
                    cell.movieDate.text = data.firstAirDate
                    cell.setVoteCountText(to: String(data.voteAverage))
                    cell.movieDescription.text = data.overview
                    cell.layer.cornerRadius = 10
                    if let url = URL(string: "https://image.tmdb.org/t/p/w500/\(data.posterPath ?? "")") {
                        cell.moviePoster.kf.setImage(with: url, placeholder: UIImage(named: "TMDb"))
                    }
                   
                }
                return cell
            })
    }
    
    // MARK: - Update Data Source
    private func updateDataSource() {
        var snapshot = NSDiffableDataSourceSnapshot<Section, TVShow>()
        snapshot.appendSections([.first])
        if(snapshot.numberOfItems > 0){
            snapshot.deleteAllItems()
        }
        
        snapshot.appendItems(tvShowListVM.fetchedShows)
        tvShowsDataSource.apply(snapshot, animatingDifferences: true)
    }
    
    // MARK: - Condigure CompositionalLayout
    private func configureLayout() -> UICollectionViewLayout {
            //items
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.5), heightDimension: .absolute(400))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4)
       
        //Groups
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(400))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
       
        //Section
        let section = NSCollectionLayoutSection(group: group)
        
        //Layout
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
        }
}

// MARK: - TVShowsViewModel Delegate
extension TVShowListViewController{
    func fetchSuccessful(with tvShowlist: [TVShow]) {
        updateDataSource()
        shouldFetchNextPage = true
    }
    
    func fetchFailed(with error: Error) {
        presentAlert(title: "Error", message: "The data could not be fetched.")
    }
    
    func realmFetchfailed() {
        presentAlert(title: "No More Data", message: "Unable to retrieve data from local cache. Please connect to a network and try again.")
    }
}

extension TVShowListViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (collectionView.contentOffset.y + collectionView.frame.size.height) >= collectionView.contentSize.height * 2/3,
           shouldFetchNextPage, tvShowListVM.fetchedShows.count > 0{
            shouldFetchNextPage = false
            tvShowListVM.fetchNextPage()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showTVShowDetail", sender: self.tvShowsDataSource.itemIdentifier(for: indexPath))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTVShowDetail", let destination = segue.destination as? TVShowDetailViewController, let tvShow = sender as? TVShow{
            destination.tvShowVM = TVShowDetailViewModel(tvShow: tvShow)
        }
    }
}

extension TVShowListViewController{
    func connectionDidChange(connected: Bool) {
        DispatchQueue.main.async {
            self.updateDataSource()
            switch NetworkMonitor.shared.isConnected {
            case false:
                self.connectionWarning.text = "No Internet Connection!"
                self.connectionWarning.backgroundColor = .systemRed
                self.warningHeight.isActive = true
                self.connectionWarning.frame = self.warningFrame
                self.spaceToWarningLabel.isActive = true
                self.connectionWarning.fadeIn()
            case true:
                UIView.animate(withDuration: 0.5, animations: {
                    self.connectionWarning.text = "Connected!"
                    self.connectionWarning.backgroundColor = #colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1)
                }, completion: { completion in
                    self.connectionWarning.fadeOut(delay: 0.5, onCompletion: {
                        self.warningHeight.isActive = false
                        self.connectionWarning.frame = CGRect.zero
                        self.spaceToWarningLabel.isActive = false
                    })
                    
                })
            }
        }
    }
}
