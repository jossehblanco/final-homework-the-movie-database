//
//  FavouriteListViewController.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/21/21.
//

import UIKit
import SwiftUI

class FavouriteListViewController: UIViewController{

    var favVM: FavouriteScreenViewModel!
    
    private var backgroundImage, shadowImage: UIImage?
    private var backgroundColor: UIColor?
    override func viewDidLoad() {
        super.viewDidLoad()
        let hostingController = UIHostingController(rootView: FavouritesView(viewModel: FavouriteScreenViewModel()))
        hostingController.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(hostingController)
        self.view.addSubview(hostingController.view)
        NSLayoutConstraint.activate([
            hostingController.view.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1),
            hostingController.view.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1)
        ])
        view.backgroundColor = #colorLiteral(red: 0.03921568627, green: 0.08235294118, blue: 0.1058823529, alpha: 1)
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
}
