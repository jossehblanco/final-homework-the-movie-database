//
//  AllSeasonsViewController.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/20/21.
//

import UIKit

class AllSeasonsViewController: UIViewController, UICollectionViewDelegate, NetworkMonitorDelegate{
   
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var warningHeight: NSLayoutConstraint!
    @IBOutlet var spaceToWarningLabel: NSLayoutConstraint!
    @IBOutlet weak var connectionWarning: UILabel!
    private lazy var tvShowsDataSource = createDataSource()
    private var warningFrame: CGRect = CGRect()
    var allSeasonsVM: AllSeasonsViewModel!
    
    private var shouldFetchNextPage = true
        
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.collectionViewLayout = configureLayout()
        updateDataSource()
        allSeasonsVM.delegate = self
        connectionDidChange(connected: NetworkMonitor.shared.isConnected)
        addFavouritesButton()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1294117647, green: 0.1725490196, blue: 0.1882352941, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        collectionView.delegate = self
        NetworkMonitor.shared.delegate = self
        connectionDidChange(connected: NetworkMonitor.shared.isConnected)
    }
    
    
}

// MARK: - CollectionView Data Source and Layout
extension AllSeasonsViewController {
    
    // MARK: - Creating Data Source
    private func createDataSource() -> UICollectionViewDiffableDataSource<Int, Episode>{
        let datasource = UICollectionViewDiffableDataSource<Int, Episode>(
            collectionView: collectionView,
            cellProvider: { [weak self] (collectionView, indexPath, data) -> UICollectionViewCell? in
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "episodeCell", for: indexPath) as! AllSeasonsEpisodeCollectionViewCell
                guard let self = self else{ return cell }
                if let data = self.tvShowsDataSource.itemIdentifier(for: indexPath){
                    cell.episodeName.text = data.name
                    let url = URL(string: self.allSeasonsVM.getURLForEpisode(for: data))!
                    cell.episodeImage.kf.setImage(with: url)
                }
                cell.layer.cornerRadius = 10.0
                return cell
            })
        datasource.supplementaryViewProvider = { collectionView, kind, indexPath in
            guard kind == UICollectionView.elementKindSectionHeader else {
                return nil
              }
            let view = collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier:  SeasonHeaderCollectionReusableView.reuseIdentifier,
                for: indexPath) as! SeasonHeaderCollectionReusableView
        
            guard let seasonNumber = self.tvShowsDataSource.itemIdentifier(for: indexPath)?.seasonNumber else {return view}
            view.titleLabel.text = "Season \(seasonNumber)"
              return view
        }
        
        return datasource
    }
    
    // MARK: - Update Data Source
    private func updateDataSource() {
        var snapshot = NSDiffableDataSourceSnapshot<Int, Episode>()
        if(snapshot.numberOfItems > 0){
            snapshot.deleteAllItems()
        }
        
        let sortedSeasons = allSeasonsVM.seasonDetails.sorted(by: {
            $0.seasonNumber < $1.seasonNumber
        })
        
        for index in 0..<sortedSeasons.count{
            snapshot.appendSections([index])
            snapshot.appendItems(allSeasonsVM.seasonDetails[index].episodes)
        }
        //snapshot.appendItems(tvShowListVM.seasonDetails)
            self.tvShowsDataSource.apply(snapshot, animatingDifferences: true)

    }
    
    // MARK: - Condigure CompositionalLayout
    private func configureLayout() -> UICollectionViewLayout {
        //Registering headers
        collectionView.register(SeasonHeaderCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: SeasonHeaderCollectionReusableView.reuseIdentifier)
            //items
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(250))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4)
       
        //Groups
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(250))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        //group.contentInsets = NSDirectionalEdgeInsets(top: 16, leading: 16, bottom: 16, trailing: 16)
        
        //Section
        
        let headerFooterSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(20))
        let sectionHeader =  NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerFooterSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .topLeading)
        
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .paging
        section.boundarySupplementaryItems = [sectionHeader]
        //Layout
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
        }
}

// MARK: - TVShowsViewModel Delegate
extension AllSeasonsViewController: AllSeasonsViewModelDelegate{
    func realmFetchFailed() {
        presentAlert(title: "No More Data", message: "Unable to retrieve data from local cache. Please connect to a network and try again.")
    }
    
    func fetchSuccessful() {
        updateDataSource()
    }
    
    func fetchFailed() {
        presentAlert(title: "Error", message: "The data could not be fetched.")
    }

}

extension AllSeasonsViewController {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "showEpisodeDetail", sender: self.tvShowsDataSource.itemIdentifier(for: indexPath))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEpisodeDetail", let episodeDetailVC = segue.destination as? EpisodeDetailViewController{
            guard let episode = sender as? Episode else {return}
            episodeDetailVC.episodeDetailVM = EpisodeDetailViewModel(tvShow: allSeasonsVM.tvShow, episode: episode)
        }
        
    }
}

extension AllSeasonsViewController{
    func connectionDidChange(connected: Bool) {
        DispatchQueue.main.async {
            self.updateDataSource()
            switch NetworkMonitor.shared.isConnected {
            case false:
                self.connectionWarning.text = "No Internet Connection!"
                self.connectionWarning.backgroundColor = .systemRed
                self.warningHeight.isActive = true
                self.connectionWarning.frame = self.warningFrame
                self.spaceToWarningLabel.isActive = true
                self.connectionWarning.fadeIn()
            case true:
                
                UIView.animate(withDuration: 0.5, animations: {
                    self.connectionWarning.text = "Connected!"
                    self.connectionWarning.backgroundColor = #colorLiteral(red: 0.1176470588, green: 0.6823529412, blue: 0.3529411765, alpha: 1)
                }, completion: { completion in
                    self.connectionWarning.fadeOut(delay: 0.5, onCompletion: {
                        self.warningHeight.isActive = false
                        self.connectionWarning.frame = CGRect.zero
                        self.spaceToWarningLabel.isActive = false
                    })
                    
                })
            }
        }
    }
}
