//
//  TVShowDetailViewController.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/18/21.
//

import UIKit
import SwiftUI

class TVShowDetailViewController: UIViewController{
    @IBOutlet var connectionWarning: UILabel!
    var tvShowVM: TVShowDetailViewModel!
    private var seasons: [Season]?
    private var tvShow: TVShow?
    private var backgroundImage, shadowImage: UIImage?
    private var backgroundColor: UIColor?
    override func viewDidLoad() {
        super.viewDidLoad()
        let hostingController = UIHostingController(rootView: TVShowDetailView(viewModel: tvShowVM))
        hostingController.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(hostingController)
        self.view.addSubview(hostingController.view)
        NSLayoutConstraint.activate([
            hostingController.view.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1),
            hostingController.view.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1)
        ])
        
        hostingController.rootView.parent = self
        view.backgroundColor = #colorLiteral(red: 0.03921568627, green: 0.08235294118, blue: 0.1058823529, alpha: 1)
        // Do any additional setup after loading the view.
        addFavouritesButton()
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        backgroundImage = navigationController?.navigationBar.backgroundImage(for: .default)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        shadowImage = navigationController?.navigationBar.shadowImage
        navigationController?.navigationBar.shadowImage = UIImage()
        backgroundColor = navigationController?.navigationBar.backgroundColor
        navigationController?.navigationBar.backgroundColor = .clear
        navigationItem.hidesBackButton = false
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    override func viewWillDisappear(_ animated: Bool) {

        navigationController?.navigationBar.setBackgroundImage(backgroundImage, for: .default)
        navigationController?.navigationBar.shadowImage = shadowImage
        navigationController?.navigationBar.backgroundColor = backgroundColor
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1294117647, green: 0.1725490196, blue: 0.1882352941, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func segueFromSwiftUI(seasons: [Season], tvshow: TVShow){
        self.seasons = seasons
        self.tvShow = tvshow
        performSegue(withIdentifier: "showAllSeasons", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAllSeasons", let allSeasonsVC = segue.destination as? AllSeasonsViewController{
            guard let tvShow = self.tvShow, let seasons = self.seasons else {return}
            
            allSeasonsVC.allSeasonsVM = AllSeasonsViewModel(tvShow: tvShow, seasons: seasons)
        }
    }
    
}
