//
//  EpisodeDetailViewController.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/20/21.
//

import UIKit
import SwiftUI

class EpisodeDetailViewController: UIViewController {
    var episodeDetailVM: EpisodeDetailViewModel!
    private var backgroundImage, shadowImage: UIImage?
    private var backgroundColor: UIColor?
    override func viewDidLoad() {
        super.viewDidLoad()
        let hostingController = UIHostingController(rootView: EpisodeDetailView(viewModel: episodeDetailVM))
        hostingController.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(hostingController)
        self.view.addSubview(hostingController.view)
        NSLayoutConstraint.activate([
            hostingController.view.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1),
            hostingController.view.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1)
        ])

        view.backgroundColor = #colorLiteral(red: 0.03921568627, green: 0.08235294118, blue: 0.1058823529, alpha: 1)
        // Do any additional setup after loading the view.
        addFavouritesButton()
    }

    override func viewWillAppear(_ animated: Bool) {

        backgroundImage = navigationController?.navigationBar.backgroundImage(for: .default)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        shadowImage = navigationController?.navigationBar.shadowImage
        navigationController?.navigationBar.shadowImage = UIImage()
        backgroundColor = navigationController?.navigationBar.backgroundColor
        navigationController?.navigationBar.backgroundColor = .clear
        navigationItem.hidesBackButton = false
        navigationController?.navigationBar.tintColor = UIColor.white
    }

    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.setBackgroundImage(backgroundImage, for: .default)
        navigationController?.navigationBar.shadowImage = shadowImage
        navigationController?.navigationBar.backgroundColor = backgroundColor
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1294117647, green: 0.1725490196, blue: 0.1882352941, alpha: 1)
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }

}
