//
//  LoadingSessionViewController.swift
//  TheMovieDatabase
//
//  Created by Applaudito on 6/12/21.
//

import UIKit
import Combine
import KeychainAccess
class LoadingSessionViewController: UIViewController, LoadingSessionViewModelDelegate{
    
    
    var circle = CircleSpinner()
    var loadingSessionVM: LoadingSessionViewModel = LoadingSessionViewModel()
    var session: Session?
    private var cancellables: Set<AnyCancellable> = []
    override func viewDidLoad() {
        super.viewDidLoad()
        circle.frame = CGRect(x: view.center.x-25.0, y: view.center.y, width: 50.0, height: 50.0)
        view.addSubview(circle)
        loadingSessionVM.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        circle.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        circle.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        loadingSessionVM.createLoginSession()
    }
    
    func setRequestToken(to token: RequestToken){
        loadingSessionVM.request_token = token
    }
    
    
    func sessionCreated(with session: Session) {
        self.session = session
        let keychain = Keychain(service: "com.applaudostudios.TheMovieDatabase")
        keychain["session_id"] = session.session_id
        self.performSegue(withIdentifier: "showTVShowList", sender: self)
    }
    
    func sessionFailed(with error: Error) {
        presentAlert(title: "Error", message: "Unable to log in.")
    }

}
