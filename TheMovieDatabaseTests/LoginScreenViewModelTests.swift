//
//  LoadingScreenViewModelTests.swift
//  TheMovieDatabaseTests
//
//  Created by Applaudito on 6/16/21.
//

import XCTest
@testable import TheMovieDatabase

class LoginScreenViewModelTests: XCTestCase, LoginScreenViewModelDelegate{
    var expectation:XCTestExpectation?
    var token:RequestToken?
    var requester: NetworkRequester!
    var service: APIService!
    var viewModel: LoginScreenViewModel!

    override func setUp() {
        expectation = nil
        token = nil
        requester = NetworkRequester(urlSession: MockURLSessionExecuter())
        service = APIService(networkRequester: requester)
        viewModel = LoginScreenViewModel(service: service)
        viewModel.delegate = self
    }
    
    func testLogin(){
        //Given
        expectation = XCTestExpectation(description: "Expects ViewModel Delegate Call")
        
        //When
        viewModel.requestRemoteLogin()
        wait(for: [expectation!], timeout: 5)
        
        //Then
        XCTAssertNotNil(token)
    }
    
    
    //Delegate
    func remoteLoginSuccessful(with token: RequestToken) {
        self.token = token
        expectation?.fulfill()
    }
    
    func remoteLoginFailed(with error: Error) {
        XCTFail()
    }
    

}
