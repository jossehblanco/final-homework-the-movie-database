//
//  TestAPIService.swift
//  TheMovieDatabaseTests
//
//  Created by Applaudito on 6/16/21.
//

import XCTest
@testable import TheMovieDatabase
import Combine

class TestAPIService: XCTestCase {
    var requester: NetworkRequester!
    var cancellables: Set<AnyCancellable>!
    var service: APIService!
    
    override func setUp() {
        requester = NetworkRequester(urlSession: MockURLSessionExecuter())
        cancellables = []
        service = APIService(networkRequester: requester)
    }
    
    
    func testGetRequestToken(){
        //Given
        var token:RequestToken?
        var error:Error?
        let expectation = XCTestExpectation(description: "Expects decoded value")
        
        //When
        service.getRequestToken().sink(receiveCompletion: { completion in
            switch(completion){
            case .failure(let serviceError):
                error = serviceError
                expectation.fulfill()
            case .finished:
                expectation.fulfill()
            }
        }, receiveValue: { value in
            token = value
        }).store(in: &cancellables)
        wait(for: [expectation], timeout: 5)
        
        //Then
        XCTAssertNil(error)
        XCTAssertNotNil(token)
    }
    
    func testGetSessionId(){
        //Given
        var session:Session?
        var error:Error?
        let expectation = XCTestExpectation(description: "Expects decoded value")
        
        //When
        service.getSessionId(with: "testToken").sink(receiveCompletion: { completion in
            switch(completion){
            case .failure(let serviceError):
                error = serviceError
                expectation.fulfill()
            case .finished:
                expectation.fulfill()
            }
        }, receiveValue: { value in
            session = value
        }).store(in: &cancellables)
        wait(for: [expectation], timeout: 5)
        
        //Then
        XCTAssertNil(error)
        XCTAssertNotNil(session)
    }
    
    
    func testGetPopularTVShows(){
        //Given
        var response:TVShowListResponse?
        var error:Error?
        let expectation = XCTestExpectation(description: "Expects decoded value")
        
        //When
        
        service.getTVShowList(of: .popular, at: 1).sink(receiveCompletion: { completion in
            switch(completion){
            case .failure(let apiError):
                error = apiError
                expectation.fulfill()
            case .
                    finished:
                expectation.fulfill()
            }
        }, receiveValue: { apiResponse in
            response = apiResponse
        }).store(in: &cancellables)
        wait(for: [expectation], timeout: 5)
        
        //Then
        XCTAssertNil(error)
        XCTAssertNotNil(response)
    }
    
    func testGetTopRatedTVShows(){
        //Given
        var response:TVShowListResponse?
        var error:Error?
        let expectation = XCTestExpectation(description: "Expects decoded value")
        
        //When
        
        service.getTVShowList(of: .topRated, at: 1).sink(receiveCompletion: { completion in
            switch(completion){
            case .failure(let apiError):
                error = apiError
                expectation.fulfill()
            case .
                    finished:
                expectation.fulfill()
            }
        }, receiveValue: { apiResponse in
            response = apiResponse
        }).store(in: &cancellables)
        wait(for: [expectation], timeout: 5)
        
        //Then
        XCTAssertNil(error)
        XCTAssertNotNil(response)
    }
    
    func testGetOnTVShows(){
        //Given
        var response:TVShowListResponse?
        var error:Error?
        let expectation = XCTestExpectation(description: "Expects decoded value")
        
        //When
        
        service.getTVShowList(of: .onTV, at: 1).sink(receiveCompletion: { completion in
            switch(completion){
            case .failure(let apiError):
                error = apiError
                expectation.fulfill()
            case .
                    finished:
                expectation.fulfill()
            }
        }, receiveValue: { apiResponse in
            response = apiResponse
        }).store(in: &cancellables)
        wait(for: [expectation], timeout: 5)
        
        //Then
        XCTAssertNil(error)
        XCTAssertNotNil(response)
    }
    
    func testGetAiringTodayTVShows(){
        //Given
        var response:TVShowListResponse?
        var error:Error?
        let expectation = XCTestExpectation(description: "Expects decoded value")
        
        //When
        
        service.getTVShowList(of: .airingToday, at: 1).sink(receiveCompletion: { completion in
            switch(completion){
            case .failure(let apiError):
                error = apiError
                expectation.fulfill()
            case .
                    finished:
                expectation.fulfill()
            }
        }, receiveValue: { apiResponse in
            response = apiResponse
        }).store(in: &cancellables)
        wait(for: [expectation], timeout: 5)
        
        //Then
        XCTAssertNil(error)
        XCTAssertNotNil(response)
    }
    
    func testGetTVShowDetails(){
        //Given
        var response:TVShowDetail?
        var error:Error?
        let expectation = XCTestExpectation(description: "Expects decoded value")
        
        //When
        service.getTVShowDetails(for: 1).sink(receiveCompletion: { completion in
            switch(completion){
            case .failure(let apiError):
                error = apiError
                expectation.fulfill()
            case .finished:
                expectation.fulfill()
            }
        }, receiveValue: { apiResponse in
            response = apiResponse
        }).store(in: &cancellables)
        wait(for: [expectation], timeout: 5)
        
        //Then
        XCTAssertNil(error)
        XCTAssertNotNil(response)
    }
    
    
    func testGetTVShowCredits(){
        //Given
        var response:Credit?
        var error:Error?
        let expectation = XCTestExpectation(description: "Expects decoded value")
        
        //When
        service.getTVShowCredits(for: 1).sink(receiveCompletion: { completion in
            switch(completion){
            case .failure(let apiError):
                error = apiError
                expectation.fulfill()
            case .finished:
                expectation.fulfill()
            }
        }, receiveValue: { apiResponse in
            response = apiResponse
        }).store(in: &cancellables)
        wait(for: [expectation], timeout: 1)
        
        //Then
        XCTAssertNil(error)
        XCTAssertNotNil(response)
    }
    
    func testGetSeasonDetails(){
        //Given
        var response:SeasonDetail?
        var error:Error?
        let expectation = XCTestExpectation(description: "Expects decoded value")
        
        //When
        service.getSeasonDetails(of: 1, for: 1).sink(receiveCompletion: { completion in
            switch(completion){
            case .failure(let apiError):
                error = apiError
                expectation.fulfill()
            case .finished:
                expectation.fulfill()
            }
        }, receiveValue: { apiResponse in
            response = apiResponse
        }).store(in: &cancellables)
        wait(for: [expectation], timeout: 1)
        
        //Then
        XCTAssertNil(error)
        XCTAssertNotNil(response)
    }

    func testGetEpisodeDetails(){
        //Given
        var response:EpisodeDetail?
        var error:Error?
        let expectation = XCTestExpectation(description: "Expects decoded value")
        
        //When
        service.getEpisodeDetails(tvID: 1, seasonNumber: 1, episodeNumber: 1).sink(receiveCompletion: { completion in
            switch(completion){
            case .failure(let apiError):
                error = apiError
                expectation.fulfill()
            case .finished:
                expectation.fulfill()
            }
        }, receiveValue: { apiResponse in
            response = apiResponse
        }).store(in: &cancellables)
        wait(for: [expectation], timeout: 1)
        
        //Then
        XCTAssertNil(error)
        XCTAssertNotNil(response)
    }
    
}
