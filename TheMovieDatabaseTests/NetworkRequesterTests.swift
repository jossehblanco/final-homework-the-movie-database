//
//  NetworkRequesterTests.swift
//  TheMovieDatabaseTests
//
//  Created by Applaudito on 6/15/21.
//

import XCTest
import Combine
@testable import TheMovieDatabase

class NetworkRequesterTests: XCTestCase {
    var requester: NetworkRequester!
    var cancellables: Set<AnyCancellable>!
    
    override func setUp() {
        requester = NetworkRequester(urlSession: MockURLSessionExecuter())
        cancellables = []
    }
    
    func testGetData(){
        //Given
        var value: Data?
        var error: Error?
        let url = "https://www.tests.org/testdata"
        let expectation = XCTestExpectation(description: "Expects Response")
        //When
        requester.requestData(from: url).sink(receiveCompletion: { completion in
            switch(completion){
            case .failure(let callError):
                error = callError
                expectation.fulfill()
            case .finished:
                expectation.fulfill()
            }
        }, receiveValue: { data in
            value = data
        }).store(in: &cancellables)
        wait(for: [expectation], timeout: 10)
        //Then
        XCTAssertNil(error)
        XCTAssertNotNil(value)
    }
    
    
    
    func testPostData(){
        //Given
        var value: Data?
        var error: Error?
        let url = "https://www.tests.org/testdata"
        let expectation = XCTestExpectation(description: "Expects Response")
        //When
        requester.postRequest(to: url, with: [:]).sink(receiveCompletion: { completion in
            switch(completion){
            case .failure(let callError):
                error = callError
                expectation.fulfill()
            case .finished:
                expectation.fulfill()
            }
        }, receiveValue: { data in
            value = data
        }).store(in: &cancellables)
        wait(for: [expectation], timeout: 5)
        //Then
        XCTAssertNil(error)
        XCTAssertNotNil(value)
    }

}
