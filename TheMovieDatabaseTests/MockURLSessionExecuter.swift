//
//  MockURLSession.swift
//  TheMovieDatabaseTests
//
//  Created by Applaudito on 6/15/21.
//

import Foundation
import Combine
@testable import TheMovieDatabase

enum TestError: Error{
    case error
}

class MockURLSessionExecuter: URLSessionExecuter{
    override func execute(for url: String) -> AnyPublisher<Data, Error> {
        var endpoint = url.components(separatedBy: ".org")[1]
        if(endpoint.contains("?")){
            endpoint = endpoint.components(separatedBy: "?")[0]
        }
        let endpointData = SampleData.endpoints[endpoint]
        return Just(endpointData!).setFailureType(to: Error.self).eraseToAnyPublisher()
    }
    
    override func executePost(for url: String, parameters: [String : Any]) -> AnyPublisher<Data, Error> {
        var endpoint = url.components(separatedBy: ".org")[1]
        if(endpoint.contains("?")){
            endpoint = endpoint.components(separatedBy: "?")[0]
        }
        let endpointData = SampleData.endpoints[endpoint]
        return Just(endpointData!).setFailureType(to: Error.self).eraseToAnyPublisher()
    }
}
