//
//  LoadingSessionViewModelTests.swift
//  TheMovieDatabaseTests
//
//  Created by Applaudito on 6/16/21.
//

import XCTest
@testable import TheMovieDatabase

class LoadingSessionViewModelTests: XCTestCase, LoadingSessionViewModelDelegate{
    
    
    var expectation:XCTestExpectation?
    var session:Session?
    var requester: NetworkRequester!
    var service: APIService!
    var viewModel: LoadingSessionViewModel!

    override func setUp() {
        expectation = nil
        session = nil
        requester = NetworkRequester(urlSession: MockURLSessionExecuter())
        service = APIService(networkRequester: requester)
        viewModel = LoadingSessionViewModel(service: service)
        viewModel.delegate = self
    }
    
    func testCreateSession(){
        //Given
        expectation = XCTestExpectation(description: "Expects ViewModel Delegate Call")
        
        //When
        viewModel.createLoginSession()
        wait(for: [expectation!], timeout: 5)
        
        //Then
        XCTAssertNotNil(session)
    }
    
    func sessionCreated(with session: Session) {
        self.session = session
        expectation?.fulfill()
    }
    
    func sessionFailed(with error: Error) {
        XCTFail()
    }
    

}
